.. Hello Python Sphinx documentation master file, created by
   sphinx-quickstart on Wed Jan  9 13:26:46 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hello Python Sphinx's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Hello controller
=====================
.. automodule:: hello_sphinx.toolbox
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
