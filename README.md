# Hello Python Sphinx

Python Sphinx demo project, based on https://gitlab.com/pages/sphinx.


## Code

### Prepare
```
poetry install
```

### Test
```
poetry run pytest
```

## Documentation

### Prepare
```
sphinx-quickstart
# configure
```

### Build
```
make html
```

### Explore
See the contents of `docs/build/html`
