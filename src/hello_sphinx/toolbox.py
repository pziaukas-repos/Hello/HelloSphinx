'''
A dummy toolbox module
'''

def say_hello(name: str) -> None:
	'''
	Prints a simple hello. That's it.
	'''
	print(f'Hello {name}!')


if __name__ == '__main__':
	say_hello('World')
